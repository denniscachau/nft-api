'use strict';

/**
 * Module dependencies
 */

// Public node modules.
const fs = require('fs');
const path = require('path');
const { PayloadTooLargeError } = require('@strapi/utils').errors;

module.exports = {
  init({ sizeLimit = 1000000 } = {}) {
    const verifySize = file => {
      if (file.size > sizeLimit) {
        throw new PayloadTooLargeError();
      }
    };
    const verifyDirectoryExists = (publicDir, layersDir) => {
      if (!fs.existsSync(path.join(publicDir, layersDir))) {
        fs.mkdirSync(path.join(publicDir, layersDir), { recursive: true });
        // fs.rmdirSync(buildDir, { recursive: true });
      }
    }

    const publicDir = strapi.dirs.public;

    return {
      async upload(file) {
        const query = await strapi.store({ type: 'controller', name: 'upload-nft', key: 'query' }).get()
        const { uuid, layerId } = query;

        const uuidDir = `/nft/${uuid}/`;
        const layersDir = path.join(uuidDir, `layers/${layerId}/`);

        verifySize(file);
        verifyDirectoryExists(publicDir, layersDir);

        return new Promise((resolve, reject) => {
          // write file in public/assets folder
          fs.writeFile(
            path.join(publicDir, layersDir, `${file.hash}${file.ext}`),
            file.buffer,
            err => {
              if (err) {
                return reject(err);
              }

              file.url = path.join(layersDir, `${file.hash}${file.ext}`);

              resolve();
            }
          );
        });
      },
      async delete(file) {
        const query = await strapi.store({ type: 'controller', name: 'upload-nft', key: 'query' }).get()
        const { uuid, layerId } = query;

        const uuidDir = `/nft/${uuid}/`;
        const layersDir = path.join(uuidDir, `layers/${layerId}/`);

        return new Promise((resolve, reject) => {
          const filePath = path.join(publicDir, layersDir, `${file.hash}${file.ext}`);

          if (!fs.existsSync(filePath)) {
            return resolve("File doesn't exist");
          }

          // remove file from public/assets folder
          fs.unlink(filePath, err => {
            if (err) {
              return reject(err);
            }

            resolve();
          });
        });
      },
    };
  },
};
