import { Stripe } from "stripe";

/**
 * checkout service.
 */
module.exports = () => ({
  async createCheckoutSession(stripePriceId, stripePlanId, isSubscription, productId, productName, metadata) {
    const pluginStore = strapi.store({
      environment: strapi.config.environment,
      type: 'plugin',
      name: 'stripe-custom',
    });
    const stripeSettings = await pluginStore.get({ key: 'stripeSetting' });
    let stripe;
    if (stripeSettings.isLiveMode) {
      stripe = new Stripe(stripeSettings.stripeLiveSecKey, {
        apiVersion: null
      });
    } else {
      stripe = new Stripe(stripeSettings.stripeTestSecKey, {
        apiVersion: null
      });
    }
    let priceId;
    let paymentMode;
    if (isSubscription) {
      priceId = stripePlanId;
      paymentMode = 'subscription';
    } else {
      priceId = stripePriceId;
      paymentMode = 'payment';
    }
    return await stripe.checkout.sessions.create({
      line_items: [
        {
          // Provide the exact Price ID (for example, pr_1234) of the product you want to sell
          price: priceId,
          quantity: 1,
        },
      ],
      mode: paymentMode,
      payment_method_types: ['card'],
      success_url: `${stripeSettings.checkoutSuccessUrl}?sessionId={CHECKOUT_SESSION_ID}`,
      cancel_url: `${stripeSettings.checkoutCancelUrl}`,
      metadata: {
        productId: `${productId}`,
        productName: `${productName}`,
        ...metadata
      },
    });
  },
});
