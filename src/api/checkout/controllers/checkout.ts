/**
 * A set of functions called "actions" for `checkout`
 */

export default {
  async createCheckoutSession(ctx) {
    // TODO only if the save is not already premium
    const {
      stripePriceId,
      stripePlanId,
      isSubscription,
      productId,
      productName,
      ...metadata
    } =
      ctx.request.body;

    await strapi
      .service('api::save.save')

    const checkoutSessionResponse = await strapi
      .service('api::checkout.checkout')
      .createCheckoutSession(stripePriceId, stripePlanId, isSubscription, productId, productName, metadata);
    ctx.send(checkoutSessionResponse, 200);
  },
  async findOne(ctx) {
    await strapi.controller('plugin::stripe-custom.stripeController').findOne(ctx);
  },
  async retrieveCheckoutSession(ctx) {
    await strapi.controller('plugin::stripe-custom.stripeController').retrieveCheckoutSession(ctx);
  },
  async savePayment(ctx) {
    const { transactionId, uuid, generation_max } = ctx.request.body;
    const exists = await strapi.query('plugin::stripe-custom.stripe-custom-payment').findOne({
      where: { transactionId  },
    });

    if (!exists) {
      console.log('create a payment');
      const savePaymentDetails = await strapi.controller('plugin::stripe-custom.stripeController').savePayment(ctx);
      // TODO Update the save with premium: true
      await strapi.service('api::save.save').markAsPremium(uuid)
      return savePaymentDetails;
    } else {
      console.log('payment already exists');
      return true;
    }
  },
};
