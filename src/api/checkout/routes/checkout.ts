const uid = "checkout";

export default {
  routes: [
    {
      method: 'GET',
      path: '/checkout/getProduct/:id',
      handler: `${uid}.findOne`,
      config: {
        auth: false,
      },
    },
    {
      method: 'POST',
      path: '/checkout/createCheckoutSession',
      handler: `${uid}.createCheckoutSession`,
      config: {
        auth: false,
      },
    },
    {
      method: 'GET',
      path: '/checkout/retrieveCheckoutSession/:id',
      handler: `${uid}.retrieveCheckoutSession`,
      config: {
        auth: false,
      },
    },
    {
      method: 'POST',
      path: '/checkout/stripePayment',
      handler: `${uid}.savePayment`,
      config: {
        auth: false,
      },
    },
  ],
};
