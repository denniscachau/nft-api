import { factories } from "@strapi/strapi";
import { transformParamsToQuery } from "@strapi/strapi/lib/services/entity-service/params";
import { v4 } from "uuid"
import { ApiSaveSave } from '../../../../schemas';

/**
 * save service.
 */
export default factories.createCoreService<any>('api::save.save', ({ strapi }) => ({
  async create(params) {
    params.request = {
      query: {
        populate: [ 'layers', 'layers.items', 'layers.items.file', 'user' ]
      }
    }

    const user = params.data.userId;
    params.data = await this.findByUuid(process.env.BASE_SAVE_UUID, params) ?? {
      layers: [
        { name: "Background", items: [] }
      ]
    };
    delete params.data.id;
    params.data.generated = false;
    params.data.generated_at = null;
    params.data.user = user;

    params.data.layers.forEach(layer => {
      delete layer.id;
      layer.items.forEach(item => delete item.id)
    })

    params.data.uuid = v4();
    params.data.name = params.data.uuid;

    return super.create(params);
  },
  async findByUuid(entityUuid, ctx) {
    const fetchParams = this.getFetchParams(ctx.request.query);
    const query = transformParamsToQuery("api::save.save", fetchParams);

    if (typeof query.where === "undefined") {
      query.where = {}
    }

    return strapi.db.query("api::save.save").findOne(
      {
        ...query,
        where: {
          uuid: entityUuid,
          ...query.where
        }
      }
    );
  },
  async updateByUuid(entityUuid, ctx) {
    const fetchParams = this.getFetchParams(ctx.request.query);
    const query = transformParamsToQuery("api::save.save", fetchParams);
    return strapi.db.query("api::save.save").findOne(
      {
        ...query,
        where: {
          uuid: entityUuid
        }
      }
    );
  },
  async delete(entityId) {
    return strapi.db.query("api::save.save").update(
      {
        where: {
          id: entityId
        },
        data: {
          deleted: true
        }
      }
    );
  },
  async markAsGenerated(uuid) {
    return strapi.db.query("api::save.save").update({
      where: {
        uuid: uuid
      },
      data: {
        generated: true,
        generated_at: new Date()
      }
    });
  },
  async markAsPremium(uuid, generation_max) {
    return strapi.db.query("api::save.save").update({
      where: {
        uuid: uuid
      },
      data: {
        premium: true,
        premium_at: new Date(),
        generation_max
      }
    });
  }
}));
