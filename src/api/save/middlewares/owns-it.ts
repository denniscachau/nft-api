/**
 * `owns-it` middleware.
 */

export default (config, { strapi }) => {
  return async (ctx, next) => {
    if (typeof ctx.request.query.filters === "undefined") {
      ctx.request.query.filters = {};
    }

    ctx.request.query.filters = Object.assign(
      ctx.request.query.filters,
      {
        user: {
          id: ctx.state.auth.credentials !== null ? ctx.state.user.id : null
        },
        deleted: false
      }
    );

    await next();
  };
};
