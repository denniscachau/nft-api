/**
 * save router.
 */

// module.exports = createCoreRouter('api::save.save');
// return

const uid = "api::save.save";

export default {
  routes: [
    {
      method: 'GET',
      path: `/saves`,
      handler: `${uid}.find`,
      config: {},
    },
    {
      method: 'GET',
      path: `/saves/uuid`,
      handler: `${uid}.findByUuid`,
      config: {
        policies: [ 'has-uuid' ],
        middlewares: [ 'api::save.owns-it' ]
      },
    },
    {
      method: 'POST',
      path: `/saves`,
      handler: `${uid}.create`,
      config: {},
    },
    {
      method: 'PUT',
      path: `/saves/:id`,
      handler: `${uid}.update`,
      config: {
        middlewares: [ 'api::save.owns-it' ]
      },
    },
    {
      method: 'PUT',
      path: `/saves/uuid`,
      handler: `${uid}.updateByUuid`,
      config: {
        policies: [ 'has-uuid' ],
        middlewares: [ 'api::save.owns-it' ]
      },
    },
    {
      method: 'PATCH',
      path: `/saves/premium`,
      handler: `${uid}.premium`,
      config: {
        policies: [ 'has-uuid' ],
        middlewares: [ 'api::save.owns-it' ]
      },
    },
    {
      method: 'DELETE',
      path: `/saves/:id`,
      handler: `${uid}.delete`,
      config: {
        middlewares: [ 'api::save.owns-it' ]
      },
    }
  ]
};
