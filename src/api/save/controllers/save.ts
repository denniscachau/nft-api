import { factories } from '@strapi/strapi';
import { ValidationError } from '@strapi/utils';
import { isObject } from 'lodash/fp';

import { parseBody } from "@strapi/strapi/lib/core-api/controller/transform";

/**
 *  save controller
 */

export default factories.createCoreController('api::save.save', ({ strapi }) => ({
  async create(ctx) {
    if (ctx.state.auth.credentials !== null) {
      ctx.request.body.data.userId = ctx.state.user.id;
    }

    return super.create(ctx);
  },
  async find(ctx) {
    const entries = await strapi.entityService.findMany('api::save.save', {
      filters: {
        $and: [
          {
            user: {
              id: ctx.state.user.id
            },
          },
          {
            deleted: false,
          }
        ],
      },
    });
    const sanitizedEntity = await this.sanitizeOutput(entries, ctx);

    return this.transformResponse(sanitizedEntity);
  },
  async findByUuid(ctx) {
    const { uuid } = ctx.request.query;

    const entity = await strapi.service('api::save.save').findByUuid(uuid, ctx);
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);

    return this.transformResponse(sanitizedEntity);
  },
  async update(ctx) {
    const save = ctx.request.body.data;

    for (let layer of save.layers) {
      for (let item of layer.items) {
        item.rarity = item.rarity > 0 ? item.rarity : 1;
      }
    }

    return await super.update(ctx);
  },
  async premium(ctx) {
    const { uuid } = ctx.request.query;

    const entity = await strapi
      .service('api::save.save')
      .markAsPremium(uuid);
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);

    return this.transformResponse(sanitizedEntity);
  },
  async updateByUuid(ctx) {
    const { uuid } = ctx.request.query;
    // const { id } = ctx.params;
    const { query } = ctx.request;

    const { data, files } = parseBody(ctx);

    if (!isObject(data)) {
      throw new ValidationError('Missing "data" payload in the request body');
    }

    const sanitizedInputData = await this.sanitizeInput(data, ctx);

    const entity = await strapi
      .service('api::save.save')
      .updateByUuid(uuid, { ...query, data: sanitizedInputData, files });
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);

    return this.transformResponse(sanitizedEntity);
  }
}));
