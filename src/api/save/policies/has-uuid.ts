/**
 * `has-uuid` policy.
 */

export default (policyContext) => {
  return typeof policyContext.request.query.uuid !== "undefined" && policyContext.request.query.uuid.length > 0;
};
