/**
 * A set of functions called "actions" for `nft`
 */

export default ({ strapi }) => ({
  "generate-legacy": async (ctx) => {
    if (process.env.GENERATE === "1") {
      await strapi.service('api::nft.nft-legacy').buildSetup(ctx);
      await strapi.service('api::nft.nft-legacy').startCreating();
    } else {
      return ctx.request.body;
    }
  },

  generate: async (ctx) => {
    ctx.request.query.populate = [ 'layers', 'layers.items', 'layers.items.file' ]
    const { uuid } = ctx.request.query;

    let config;

    const generatedSave = await strapi.service('api::save.save').markAsGenerated(uuid);

    /**
     * Get the save
     */
    strapi.service('api::save.save')
      .findByUuid(uuid, ctx)
      .then(save => {
        /**
         * Get the config using the request body
         */
        config = strapi.service('api::nft.nft').getConfig(ctx); // resolution, nb, email?

        /**
         * Build the setup
         */
        const error = strapi.service('api::nft.nft').setup({ uuid, config, save, ctx });

        if (error) {
          return error;
        }

        /**
         * Get the images paths from the save
         */
        const layers = strapi.service('api::nft.nft').getImages(save);


        return strapi.service('api::nft.nft').generate({
          layers,
          config,
          uuid
        })
      })
      .then(build => strapi.service('api::nft.nft').toZip(uuid, build))
      // .then(file => strapi.service('api::nft.nft').sendEmail(file, { config }))

    return generatedSave;
  },
  preview: async (ctx) => {
    ctx.request.query.populate = [ 'layers', 'layers.items', 'layers.items.file' ]
    const { uuid } = ctx.request.query;

    /**
     * Get the save
     */
    const save = await strapi.service('api::save.save').findByUuid(uuid, ctx);

    /**
     * Build the setup
     */
    strapi.service('api::nft.nft').setup({ uuid, config: { nb: 1 }, save, ctx});

    /**
     * Get the config using the request body
     */
    const config = strapi.service('api::nft.nft').getConfig(ctx); // resolution, nb, email?
    config.nb = 1;

    /**
     * Get the images paths from the save
     */
    const layers = strapi.service('api::nft.nft').getImages(save);


    const build = await strapi.service('api::nft.nft').generate({
      layers,
      config,
      uuid
    });

    const path = build[0];
    const url = path.replace(
      `${process.cwd()}/public`,
      ''
    );

    return { data: { src: url } };
  }
});
