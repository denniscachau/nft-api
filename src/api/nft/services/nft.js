'use strict';
/**
 * nft service.
 */

const { buildDir, basePath } = require("./config");
const fs = require("fs");
const MD5 = require("md5.js");
const { createCanvas, loadImage } = require(`canvas`);
const JSZip = require("jszip");

const createDna = (images) => {
  const to_dna = images.map((image) => image.layer+":"+image.path).join('-');
  return new MD5().update(to_dna).digest('hex');
}

const createGeneratedImage = async ({ images, config, index, uuid }) => {
  const canva = createCanvas(config.resolution.x, config.resolution.y);
  const ctx = canva.getContext('2d');

  let loadedImages = [];
  for (let image of images) {
    loadedImages.push(loadImage(`${basePath}/public${image.path}`));
  }

  return Promise.all(loadedImages)
    .then((generatedImages) => {
      const path = `${buildDir}/${uuid}/images/${index}.png`;
      for (let generatedImage of generatedImages) {
        generatedImage = ctx.drawImage(generatedImage, 0, 0);
      }
      fs.writeFileSync(
        path,
        canva.toBuffer("image/png")
      );

      console.log(`${index+1} images generated`);

      return path;
    });
}

const getRandom = (layer) => {
  const max_random = layer.map((l) => l.rarity).reduce((a, b) => a+b);
  const random = Math.random() * max_random;

  let cur_index = 0;
  return layer.find((l) => {
    cur_index += l.rarity;
    return random <= cur_index;
  });
}

const createMetadata = ({ collectionName, path, index, uuid, images, dna }) => {
  const pathMatch = path.replace(`${buildDir}/`, '');
  const imageUri = `${'http://www.test.com'}/build/${pathMatch}`;

  const metadata = {
    name: `${collectionName} #${index}`,
    description: `${collectionName} #${index}`,
    // image: imageUri,
    edition: Number(index),
    dna,
    date: new Date().valueOf(),
    attributes: images.map(i => ({ ...i, path: undefined })),
    // compiler: "HashLips Art Engine",
  };
  fs.writeFileSync(
    `${buildDir}/${uuid}/metadata/${index}.json`,
    JSON.stringify(metadata, null, 2)
  );
  return metadata;
}

module.exports = ({ strapi }) => ({
  setup: ({ uuid, config, save, ctx }) => {
    if (!(config.nb <= save.generation_max) && config.nb <= 50) {
      return ctx.badRequest(`can't generate more than ${save.generation_max}`, { max: save.generation_max })
    }

    if (fs.existsSync(`${buildDir}/${uuid}`)) {
      fs.rmdirSync(`${buildDir}/${uuid}`, { recursive: true });
    }
    fs.mkdirSync(`${buildDir}/${uuid}`);
    fs.mkdirSync(`${buildDir}/${uuid}/metadata`);
    fs.mkdirSync(`${buildDir}/${uuid}/images`);
  },
  getConfig: ({ request }) => {
    return { ...request.body, maxGeneratedDna: 20000};
  },
  getImages: (save) => {
    let images = {};
    for (let layer of save.layers) {
      images[layer.name] = [];

      for (let item of layer.items) {
        images[layer.name].push({...item, path: item.file.url})
      }
    }

    return images;
  },
  generate: async ({ layers, config, uuid }) => {
    let dnas = [];
    let dna = null;

    /**
     * Selected images
     * @type string[]
     */
    let images;

    /**
     * nb generated images
     * @type number
     */
    let i = 0;

    let imageMetadata = [];

    let paths = [];

    console.log(`Start generating for the UUID: ${uuid}`);
    const extRegExp = new RegExp(/\.\w+$/);
    generateImages: {
      /**
       * Do while the max generated images is not acheived
       * Or no more images can be created
       */
      do {
        let j = 0;
        // do while the dna exists
        do {
          j++;
          images = [];
          for (let layer of Object.keys(layers)) {
            const image = getRandom(layers[layer]);
            images.push({
              path: image.path,
              layer: layer,
              value: image.name.replace(extRegExp, ""),
              rarity: `${image.rarity}%`
              }
            )
          }
          dna = createDna(images);

          if (j >= config.maxGeneratedDna) {
            console.error(`Too many attempts for this collection. ${i} images has been generated (${j} attempts can't be exceeded)`)
            break generateImages;
          }
        } while (dnas.includes(dna));

        dnas.push(dna);

        const path = await createGeneratedImage({
          images,
          config,
          index: i,
          uuid
        });

        paths.push(path);

        imageMetadata.push(createMetadata({ collectionName: config.name, index: i, path, uuid, images, dna }))

        i++;
      } while (i < config.nb)
    }

    /**
     * TODO create global metadata
     */

    return paths;
  },
  toZip: async (uuid, files) => {
    const zip = new JSZip();

    for (let file of files) {
      const fileData = fs.readFileSync(file);
      zip.file(file.split('\\').pop().split('/').pop(), fileData);
    }

    return zip.generateNodeStream({ type: 'nodebuffer', streamFiles: true })
      .pipe(fs.createWriteStream(`${buildDir}/${uuid}/images.zip`))
  },
  sendEmail: async (file, { config }) => {
    const url = process.env.BASE_URL.concat(file.path.replace(
      `${process.cwd()}/public`,
      ''
    ));
    return strapi.plugins['email'].services.email.send({
      to: config.email,
      text: `Here is the link to download your nft images: ${url}`,
      html: `<p>
              Click <a href="${url}">here</a> to download your nft images.
            </p>`,
      subject: "Download link for your NFT images"
    })
  }
});
