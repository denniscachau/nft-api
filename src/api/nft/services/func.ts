import * as MD5 from "md5.js";

const createDna = (images) => {
  const to_dna = images.map((image) => image.layer+":"+image.path).join('-');
  return new MD5().update(to_dna).digest('hex');
}

export default {
  createDna
}
