'use strict';
/**
 * nft service.
 */

const {
  fs,
  sha1,
  buildDir,
  format,
  uniqueDnaTorrance,
  layerConfigurations,
  layersDirectory,
  shuffleLayerConfigurations,
  debugLogs,
  // gif,
  // canvas,
  ctx,
  metadataList,
  dnaList,
  // HashlipsGiffer,
  // hashlipsGiffer,
  // getRarityWeight,
  // cleanName,
  saveImage,
  loadLayerImg,
  layersSetup,
  drawElement,
  constructLayerToDna,
  filterDNAOptions,
  isDnaUnique,
  createDna,
  writeMetaData,
  shuffle
} = require('./func-legacy');

let layersConfig;
let layersDir;

module.exports = ({ strapi }) => ({
  buildSetup: async (ctx) => {
    ctx.request.query.populate = [ 'layers', 'layers.items', 'layers.items.file' ]
    const { uuid } = ctx.request.query;
    layersConfig = [await layerConfigurations(strapi, uuid, ctx)];
    layersDir = layersDirectory(uuid);

    if (fs.existsSync(buildDir)) {
      fs.rmdirSync(buildDir, { recursive: true });
    }
    fs.mkdirSync(buildDir);
    fs.mkdirSync(`${buildDir}/json`);
    fs.mkdirSync(`${buildDir}/images`);
    // if (gif.export) {
    //   fs.mkdirSync(`${buildDir}/gifs`);
    // }
  },
  startCreating: async () => {
    let layerConfigIndex = 0;
    let editionCount = 1;
    let failedCount = 0;
    let abstractedIndexes = [];
    for (let i = 0; i <= layersConfig[layersConfig.length - 1].growEditionSizeTo; i++) {
      abstractedIndexes.push(i);
    }
    if (shuffleLayerConfigurations) {
      abstractedIndexes = shuffle(abstractedIndexes);
    }
    debugLogs
      ? console.log("Editions left to create: ", abstractedIndexes)
      : null;
    console.log(layersConfig[layerConfigIndex].layersOrder);
    while (layerConfigIndex < layersConfig.length) {
      const layers = layersSetup(
        layersDir,
        layersConfig[layerConfigIndex].layersOrder
      );
      while (
        editionCount <= layersConfig[layerConfigIndex].growEditionSizeTo
        ) {
        let newDna = createDna(layers);
        if (isDnaUnique(dnaList, newDna)) {
          let results = constructLayerToDna(newDna, layers);
          let loadedElements = [];

          results.forEach((layer) => {
            loadedElements.push(loadLayerImg(layer));
          });

          await Promise.all(loadedElements).then((renderObjectArray) => {
            debugLogs ? console.log("Clearing canvas") : null;
            ctx.clearRect(0, 0, format.width, format.height);
            // if (gif.export) {
            //   hashlipsGiffer = new HashlipsGiffer(
            //     canvas,
            //     ctx,
            //     `${buildDir}/gifs/${abstractedIndexes[0]}.gif`,
            //     gif.repeat,
            //     gif.quality,
            //     gif.delay
            //   );
            //   hashlipsGiffer.start();
            // }
            // if (background.generate) {
            //   drawBackground();
            // }
            renderObjectArray.forEach((renderObject, index) => {
              drawElement(
                renderObject,
                index,
                layersConfig[layerConfigIndex].layersOrder.length
              );
              // if (gif.export) {
              //   hashlipsGiffer.add();
              // }
            });
            // if (gif.export) {
            //   hashlipsGiffer.stop();
            // }
            debugLogs
              ? console.log("Editions left to create: ", abstractedIndexes)
              : null;
            saveImage(abstractedIndexes[0]);
            // addMetadata(newDna, abstractedIndexes[0]);
            // saveMetaDataSingleFile(abstractedIndexes[0]);
            console.log(
              `Created edition: ${abstractedIndexes[0]}, with DNA: ${sha1(
                newDna
              )}`
            );
          });
          dnaList.add(filterDNAOptions(newDna));
          editionCount++;
          abstractedIndexes.shift();
        } else {
          console.log("DNA exists!");
          failedCount++;
          if (failedCount >= uniqueDnaTorrance) {
            console.log(
              `You need more layers or elements to grow your edition to ${layersConfig[layerConfigIndex].growEditionSizeTo} artworks!`
            );
            // process.exit();
            return;
          }
        }
      }
      layerConfigIndex++;
    }
    writeMetaData(JSON.stringify(metadataList, null, 2));
  }
});
