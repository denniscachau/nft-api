'use strict';

const basePath = process.cwd();
const rarityDelimiter = '#--';
const shuffleLayerConfigurations = false;
const debugLogs = false;
const uniqueDnaTorrance = 20;
const buildDir = `${basePath}/public/build`;

module.exports = {
  async layerConfigurations(strapi, uuid, ctx) {
    return {
      growEditionSizeTo: ctx.request.body.nb ?? 10,
      layersOrder: (await strapi.service('api::save.save')
        .findByUuid(uuid, ctx)).layers
        .filter((layer) => layer.items.length > 0)
        .map((layer) => ({ name: layer.id }))

    }
  },
  layersDirectory(uuid) {
    return `${basePath}/public/nft/${uuid}/layers`
  },
  rarityDelimiter,
  shuffleLayerConfigurations,
  debugLogs,
  format: {
    width: 512,
    height: 512,
    smoothing: false,
  },
  uniqueDnaTorrance,
  basePath,
  buildDir
}
