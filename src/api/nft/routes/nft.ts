export default {
  routes: [
    {
     method: 'POST',
     path: '/nft/generate',
     handler: 'nft.generate',
     config: {},
    },
    {
     method: 'POST',
     path: '/nft/preview',
     handler: 'nft.preview',
     config: {},
    },
    {
     method: 'POST',
     path: '/nft/generate-legacy',
     handler: 'nft.generate-legacy',
     config: {},
    },
  ],
};
