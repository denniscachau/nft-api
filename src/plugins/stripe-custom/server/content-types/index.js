'use strict';

const productSchema = require('./product');
const paymentSchema = require('./payment');

module.exports = {
  'stripe-custom-product': { schema: productSchema }, //// should re-use the singularName of the content-type
  'stripe-custom-payment': { schema: paymentSchema },
};
