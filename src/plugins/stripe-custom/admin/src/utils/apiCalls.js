import instance from './axiosInstance';

const axios = instance;

export async function saveStripeConfiguration(data) {
  const response = await axios.put('/stripe-custom/updateSettings', {
    data,
  });

  return response;
}

export async function getStripeConfiguration() {
  const response = await axios.get('/stripe-custom/getSettings');

  return response;
}

export async function createStripeProduct(
  title,
  price,
  imageId,
  imageUrl,
  description,
  isSubscription,
  paymentInterval,
  trialPeriodDays
) {
  const response = await axios.post('/stripe-custom/createProduct', {
    title,
    price,
    imageId,
    imageUrl,
    description,
    isSubscription,
    paymentInterval,
    trialPeriodDays,
  });

  return response;
}

export async function getStripeProduct(offset, limit, sort, order) {
  const response = await axios.get(`/stripe-custom/getProduct/${offset}/${limit}/${sort}/${order}`);

  return response;
}

export async function getStripeProductProductById(id) {
  const response = await axios.get(`/stripe-custom/getProduct/${id}`);

  return response;
}

export async function updateStripeProduct(
  id,
  title,
  url,
  description,
  productImage,
  stripeProductId
) {
  const response = await axios.put(`/stripe-custom/updateProduct/${id}`, {
    title,
    url,
    description,
    productImage,
    stripeProductId,
  });

  return response;
}

export async function getProductPayments(productId, sort, order, offset, limit) {
  const response = await axios.get(
    `/stripe-custom/getPayments/${productId}/${sort}/${order}/${offset}/${limit}`
  );

  return response;
}

export async function uploadFiles(files) {
  const formDocument = new FormData();
  formDocument.append('files', files[0]);
  const response = await axios.post(`/upload`, formDocument);

  return response;
}
