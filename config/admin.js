module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '58fe4be27a01aaacd588fa8f799cfd42')
  },
  apiToken: {
    salt: env('API_TOKEN_SALT')
  }
});
