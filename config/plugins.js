module.exports = ({ env }) => ({
  email: {
    enabled: env.bool('SEND_EMAIL', true),
    config: {
      provider: 'nodemailer',
      providerOptions: {
        host: env('SMTP_HOST'),
        port: env('SMTP_PORT', 465),
        secure: env('SMTP_SECURE', true),
        auth: {
          user: env('SMTP_USERNAME'),
          pass: env('SMTP_PASSWORD'),
        },
      },
      settings: {
        defaultFrom: 'smtp@ps-ciren.com',
        testAddress: 'dennis.cachau@gmail.com'
      }
    }
  },
  'stripe-custom': {
    enabled: true,
    resolve: './src/plugins/stripe-custom'
  },

});
